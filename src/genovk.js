import ovk from './models/ovk';
let newOvk = new ovk();

console.log('Generating OVK credentials');
newOvk.generateBadge('Boardent').then( success => {
  if (!success) throw ('error generating ovk');
  let persona = newOvk.public.personas.pop();
  const result = {
    name: persona.name,
    address: persona.address,
    badge: newOvk.public.badge,
    privateKey: newOvk.private
  };
  console.log(result);
});

import joi from 'joi';

export default class baseModel {
  constructor (db) {
    this.db = db;
    this.validator = joi;
    this.props = {};
  }
}

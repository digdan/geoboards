import baseModel from './baseModel';
import ovk from './ovk';

export default class user extends baseModel {
  constructor (db) {
    super(db);
    this.schema = this.validator.object().keys({
      username: this.validator.string(),
      address: this.validator.string(),
      displayName: this.validator.string()
    });
  }

  load(id) {
    console.log('load', id);
    this.props = {id};
    return true;
  }

  add(props) {
    this.props = props;
    let userOVK = new ovk(this.db);
    userOVK.generateBadge(this.props.username);
    this.props.ovk = userOVK.public;
    return true;
  }

  save() {
    return true;
  }

  delete() {
    return true;
  }

}

import baseModel from './baseModel';

export default class post extends baseModel {
  constructor (db) {
    super(db);
    this.schema = this.validator.object().keys({
      userId: this.validator.number().required(),
      message: this.validator.string().required(),
      longitude: this.validator.number().required(),
      latitude: this.validator.number().required()
    });
  }

  load(id) {
    console.log('load', id);
    this.props = {id};
    return true;
  }

  add(props) {
    this.props = props;
    this.save();
    return true;
  }

  save() {
    //Save to geolocaation
    //Save post content
    //Update counts
    //Update timers
    return true;
  }

  delete() {
    //Move post to archived posts list
    return true;
  }
}

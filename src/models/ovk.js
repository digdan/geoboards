import baseModel from './baseModel';
import basex from 'base-x';
import crypto2 from 'crypto2';
let base58 = basex('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz');


export default class ovk extends baseModel {
  constructor (db) {
    super(db);
    this.public = {};
    this.private = '';
  }

  createHmac(input, hasher) {
    return new Promise((resolve, reject) => {
      crypto2.hmac(input, hasher, (err, hmac) => {
        if (err) reject('unable to create hmac');
        resolve(hmac);
      })
    })
  }

  createHash(input) {
    return new Promise((resolve, reject) => {
      crypto2.hash(input, (err, hash) => {
        if (err) reject('unable to create hash');
        resolve(hash);
      });
    });
  }

  encrypt (input, pass) {
    return new Promise((resolve, reject) => {
      crypto2.encrypt.aes256cbc(input, pass, (err, encrypted) => {
        if (err) reject(err);
        resolve(encrypted);
      });
    });
  }

  decrypt(input, pass) {
    return new Promise((resolve, reject) => {
      crypto2.decrypt(input, pass, (err, decrypted) => {
        if (err) reject(err);
        resolve(decrypted);
      });
    });
  }

  genKeys() {
    return new Promise((resolve, reject) => {
      crypto2.createKeyPair( (err, privateKey, publicKey) => {
        if (err) reject(err);
        resolve({private:privateKey, public:publicKey});
      });
    });
  }

  hex2base(hex) {
    let temp = new Buffer(hex,'hex');
    return base58.encode(temp);
  }

  buffer2pem(contents, token) {
    let pem = `-----BEGIN ${token}-----\n`;
    let base64String = contents.toString('base64');
    while (base64String.length > 64) {
      pem += base64String.substr(0,64)+'\n';
      base64String = base64String.substr(64);
    }
    pem += base64String+'\n';
    pem += `-----END ${token}-----`;
    return pem;
  }

  pem2buffer(contents) {
    let base64Strings = [];
    let currentToken='';
    let buffers=[];
    const contentsArr = contents.split('\n');
    for(var i in contentsArr) {
      if (contentsArr[i].startsWith('-----BEGIN ')) {
        currentToken = contentsArr[i].substring(11,(contentsArr[i].length - 5));
        base64Strings[currentToken] = '';
      } else if (contentsArr[i].startsWith('-----END')) {
        currentToken = '';
      } else {
        base64Strings[currentToken] += contentsArr[i];
      }
    }
    for(var j in base64Strings) {
      buffers[j] = new Buffer(base64Strings[j],'base64');
    }
    return buffers;
  }

  generateBadge(name) {
    return new Promise((resolve, reject) => {
      let privateBuffers;
      let publicBuffers;
      let hmac;
      let hashAddress;

      this.genKeys().then( keyPair => {
        privateBuffers = this.pem2buffer(keyPair.private);
        publicBuffers = this.pem2buffer(keyPair.public);
        return this.createHmac(name, publicBuffers['PUBLIC KEY']);
      }).then( hmacResult => {
        hmac = hmacResult;
        return this.createHash(Buffer.concat([publicBuffers['PUBLIC KEY'], new Buffer(hmac,'hex')]));
      }).then( hashAddressResult => {
        hashAddress = hashAddressResult;
        this.public = {
          badge:base58.encode(publicBuffers['PUBLIC KEY']),
          personas: [
            {
              name,
              displayName:name,
              address: base58.encode(new Buffer(hashAddress,'hex')),
              config: {}
            }
          ],
          verifications:{}
        };
        this.private = base58.encode(privateBuffers['RSA PRIVATE KEY']);
        resolve(true);
      }).catch(err => {
        reject(err);
      })
    });
  }

}

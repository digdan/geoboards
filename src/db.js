import ioredis from 'ioredis';
  let dbState;

export default callback => {
  dbState = new ioredis(process.env.REDIS_URI);
  callback(dbState);
}

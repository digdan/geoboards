import { version } from '../../package.json';
import { Router } from 'express';
import post from '../models/post';

export default ({ config, db }) => {
	let api = Router();

  api.get('/', (req, res) => {
    let curPost = new post(db);
    curPost.load(11);
    res.json(curPost);
  });

	api.get('/add', (req, res) => {
    let newPost = new post(db);
    newPost.add({
      id:123,
      title:'title',
      lat:100,
      long:40
    });
    res.json(newPost);
	});
  api.get('/update', (req, res) => {
    res.json({ version });
  });
  api.get('/delete', (req, res) => {
		res.json({ version });
	});

	return api;
}

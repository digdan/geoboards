import { version } from '../../package.json';
import { Router } from 'express';
import user from '../models/user';

export default ({ config, db }) => {
	let api = Router();

  api.get('/', (req, res) => {
    let curUser = new user(db);
    curUser.load(11);
    res.json(curUser);
  });

	api.get('/add', (req, res) => {
    let newUser = new user(db);
    newUser.add({
      id:123,
      username:'digdan',
      password:'testing',
      lat:100,
      long:40
    });
    res.json(newUser.props);
	});
  api.get('/update', (req, res) => {
    res.json({ version });
  });
  api.get('/delete', (req, res) => {
		res.json({ version });
	});

	return api;
}
